package com.biggerplate.examples.basic;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import org.scribe.builder.*;
import org.scribe.builder.api.*;
import org.scribe.model.*;
import org.scribe.oauth.*;

import android.net.Uri;
import android.webkit.*;
import android.view.View;
import android.widget.Button;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.EditText;


public class MainActivity extends ActionBarActivity {

    private static final String NETWORK_NAME = "Biggerplate";
    private static final String AUTHORIZE_URL = "https://www.google.com/accounts/OAuthAuthorizeToken?oauth_token=";
    private static final String CALLBACK_URL = "http://localhost/";
    private static final String SCOPE = "account";
    private static final Token EMPTY_TOKEN = null;

    private String authorizationUrl;
    private Token  authRequestToken;
    private Button authButton;
    private WebView authWebView;
    private WebChromeClient authWebChromeClient;

    OAuthService service = new ServiceBuilder()
            .provider(BiggerplateApi.class)
            .apiKey("<add-your-clinet-id-here>")
            .apiSecret("<add-your-clinet-secret-here>")
            .callback(CALLBACK_URL)
            .scope(SCOPE)
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /**
         * Set the current view within the application
         */
        setContentView(R.layout.activity_main);

        authWebView = (WebView) findViewById(R.id.authWebView);
        authWebView.clearCache(true);
        authWebView.getSettings().setJavaScriptEnabled(true);
        authWebView.getSettings().setBuiltInZoomControls(true);
        authWebView.getSettings().setDisplayZoomControls(false);
        authWebView.setWebViewClient(mWebViewClient);
        authWebView.setWebChromeClient(authWebChromeClient);

        /**
         * Fetch the button object
         */
        authButton = (Button) findViewById(R.id.login);

        /**
         * Attach a listener to begin authorization
         */
        authButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                authorizationUrl = service.getAuthorizationUrl(EMPTY_TOKEN);

                /**
                 * Show the webview
                 */
                authWebView.setVisibility(View.VISIBLE);
                authWebView.bringToFront();
                authWebView.loadUrl(authorizationUrl);
            }
        });
    }

    protected void onAccessToken()
    {
        /**
         * Hide the signin button
         */
        authButton.setVisibility(View.INVISIBLE);

        /**
         * Write hte access token.
         */
        EditText textBox = (EditText) findViewById(R.id.accessTokenText);
        textBox.setText(authRequestToken.getToken());
    }

    private WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            if ((url != null) && (url.startsWith(CALLBACK_URL))) { // Override webview when user came back to CALLBACK_URL
                authWebView.stopLoading();
                authWebView.setVisibility(View.INVISIBLE); // Hide webview if necessary
                Uri uri = Uri.parse(url);
                final Verifier verifier = new Verifier(uri.getQueryParameter("code"));
                (new AsyncTask<Void, Void, Token>() {
                    @Override
                    protected Token doInBackground(Void... params) {
                        return service.getAccessToken(authRequestToken, verifier);
                    }

                    @Override
                    protected void onPostExecute(Token accessToken) {
                        authRequestToken = accessToken;
                        onAccessToken();
                    }
                }).execute();
            } else {
                super.onPageStarted(view, url, favicon);
            }
        }
    };
}
