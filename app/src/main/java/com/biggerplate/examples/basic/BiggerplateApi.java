package com.biggerplate.examples.basic;

import org.scribe.builder.api.*;
import org.scribe.extractors.*;
import org.scribe.model.*;
import org.scribe.utils.*;

/**
 * Created by rpitt on 06/04/15.
 */
public class BiggerplateApi extends DefaultApi20
{
    private static final String PROTOCOL = "http";
    private static final String ENVIRONMENT_DOMAIN = "staging.biggerplate.com";

    private static final String AUTHORIZE_URL = PROTOCOL + "://accounts." + ENVIRONMENT_DOMAIN + "/oauth/auth?client_id=%s&redirect_uri=%s&response_type=code";
    private static final String SCOPED_AUTHORIZE_URL = AUTHORIZE_URL + "&scope=%s";

    @Override
    public AccessTokenExtractor getAccessTokenExtractor()
    {
        return new JsonTokenExtractor();
    }

    /**
     * Returns the verb for the access token endpoint (defaults to GET)
     *
     * @return access token endpoint verb
     */
    public Verb getAccessTokenVerb()
    {
        return Verb.POST;
    }

    @Override
    public String getAccessTokenEndpoint()
    {
        return PROTOCOL + "://accounts." + ENVIRONMENT_DOMAIN  + "/oauth/token?grant_type=authorization_code";
    }

    @Override
    public String getAuthorizationUrl(OAuthConfig config)
    {
        // Append scope if present
        if (config.hasScope())
        {
            return String.format(SCOPED_AUTHORIZE_URL, config.getApiKey(), OAuthEncoder.encode(config.getCallback()), OAuthEncoder.encode(config.getScope()));
        }
        else
        {
            return String.format(AUTHORIZE_URL, config.getApiKey(), OAuthEncoder.encode(config.getCallback()));
        }
    }
}